lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "js_weber/version"

Gem::Specification.new do |spec|
  spec.name          = "js_weber"
  spec.version       = JsWeber::VERSION
  spec.authors       = ["foo"]
  spec.email         = ["foo"]

  spec.summary       = %q{some javascript methods for rails}
  spec.homepage      = 'https://bitbucket.org/ynweber/js_weber/'
  spec.license       = "MIT"


  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = 'https://bitbucket.org/ynweber/js_weber/src/master/'
  spec.metadata["changelog_uri"] = 'https://bitbucket.org/ynweber/js_weber/branches/'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
