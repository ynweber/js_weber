// these functions will read the children of 'source_elem'
// and set relevant events
// based on class names
//
// great for unobtrusive js
// and dynamic page changes.

// ive commented out things im not currently using


// TODO: skip a step.
// dont find('.js')
// rather find objects with attr click and setClick them.
// and same with change, etc
function readElems(source_elem) {
  elems = $( source_elem ).find('.js');
  console.log('readElems ' + elems.length);
  for (i=0;i<elems.length;i++) {
    setFunction(elems[i]);
  }
}

function setFunction(elem) {
  console.log('setFunction');
  setClick(elem);
  setChange(elem);
  setInput(elem);
  setLoad(elem);
  setUpdate(elem);
}

function setUpdate(elem) {
  update = $(elem).attr('update');
  if (update) { elem.onupdate = functize(update); }
}

function setClick(elem) {
  click = $(elem).attr('click');
  if (click) { elem.onclick = functize(click); }
}

function setChange(elem) {
  change = $(elem).attr('change');
  if (change) { elem.onchange = functize(change); }
}

function setInput(elem) {
  input = $(elem).attr('input');
  if (input) { elem.oninput = functize(input); }
}

function setLoad(elem) {
  load = $(elem).attr('load');
  console.log(load);
  if (load){ elem.onload = functize(load); }
  $(elem).trigger('onload');
  // was running function right away
  // if (load){ functize(load) }
  // but 'this' didn't work properly, now it does.
}

function functize(v) {
  return window[v];
}

// function readElems(source_elem) {
//   console.log('reading');
//   setOnClick(source_elem);
//   setOnChange(source_elem);
//   setOnLoad(source_elem);
//   setOnUnload(source_elem);
//   setOnSubmit(source_elem);
// }
// 
// function setOnClick(source_elem) {
//   console.log('set onclick');
//   var clicks = $( source_elem ).find(".js-onclick");
//   for (i=0; i<clicks.length; i++) {
//     console.log(clicks[i].text);
//     clicks[i].onclick = setFunction;
//   }
// }
// 
// function setFunction(funct_type) {
//   console.log('setFunction');
//   window[this.getAttribute('js_funct')](this);
// }

// function setOnChange(source_elem) {
//   var changes = $( source_elem ).find(".js-onchange");
//   for (i=0; i<changes.length; i++) {
//     changes[i].onchange = setSelectFunction;
//   }
// }
// 
// function setOnLoad(source_elem) {
//   console.log('set onload');
//   var onloads = $( source_elem ).find(".js-onload");
//   for (i=0; i<onloads.length; i++) {
//     runFunction(onloads[i]);
//   }
// }
// 
// // wont work correctly with turbolinks
// function setOnUnload(source_elem) {
//   var unloads = $( source_elem ).find(".js-onunload");
//   for (i=0; i < unloads.length; i++) {
//     var unloaded = unloads[i];
//     body = $( document ).find("body")[0];
//     js_unload_funct = $( unloaded ).attr("js_unload_funct");
//     body.onunload = window[js_unload_funct]($( unloaded ).attr('js_var'));
//   }
// }
// 
// function setOnSubmit(source_elem) {
//   console.log('set onsubmit');
//   var onsubmits = $( source_elem ).find(".js-onsubmit");
//   for (i=0; i<onsubmits.length; i++) {
//     onsubmits[i].onsubmit = setFunction;
//   }
// 
// }
// 
// function runFunction(elem) {
//   console.log('runFunction');
//   window[$( elem ).attr('js_funct')](elem, $( elem ).attr('js_var'));
// }

// function setFunction(funct_type) {
//   console.log('setFunction');
//   window[this.getAttribute('js_funct')](this);
// }

// function setSelectFunction() {
//   a_s = this[this.selectedIndex];
//   window[a_s.getAttribute('js_funct')](this, a_s.getAttribute('js_var'));
// }
