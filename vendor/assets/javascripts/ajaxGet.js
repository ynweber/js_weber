function ajaxGet(source_elem, successFunction, successVar, serial) {
  console.log('ajaxGet');
  console.log(serial);
  // it was running twice when catchForm was called twice.
  // .off() seems to have fixed it
  var url = $(source_elem).attr('action')+'?turbolinks=0';
  console.log(url);
  
  $.ajax({
    url : url,
    type: "GET",
    data: serial,
    success: function(data) {
      successFunction(successVar, data);
    }
  });
}
