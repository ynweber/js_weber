// load a new partial into inner-div, (alt. to link)
// used by root helper (?)
function datalistUpdate(source_elem) {
  // addvars = $(source_elem).attr('updateInner_addvars');
  addvars='&column='+$(source_elem).attr('updateInner_addvars');
  if ($(source_elem).hasClass('off')) {
    console.log('returning');
    return;
  }
  console.log('datalistUpdate');
  console.log('datalist id: ' + $(source_elem).attr('id'));
  // turning off events until success
  $(source_elem).addClass('off');
  var f = $(source_elem).closest('form');
  console.log('form id: ' + $(f).attr('id'));
  var path =$(source_elem).attr('updateInner_path');
  old_action = f.attr('action');
  f.attr('action', path);
  var serial = $(f).serialize() + addvars;
  // var ins = $('#'+$(source_elem).attr('list'));
  $(source_elem).append('<option class="loading">..loading</option>');
  ajaxGet(f, datalistUpdateSuccess, source_elem, serial);
  f.attr('action', old_action);
}


function datalistUpdateSuccess(source_elem, data) {
  console.log('updatesuccess');
  $(source_elem).removeClass('off');
  // var ins = $('#'+$(source_elem).attr('list'));
  $(source_elem).html(data);
  console.log('done');
}
