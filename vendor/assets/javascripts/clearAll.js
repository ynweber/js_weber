function clearAll() {
  console.log('clearAll');
  frm = $(this).closest('form');
  frm_elements = frm[0].elements;
  for (i = 0; i < frm_elements.length; i++)
  {
      field_type = frm_elements[i].type.toLowerCase();
      switch (field_type)
      {
      case "text":
          frm_elements[i].value = "";
          break;
      case "number":
      case "password":
      case "textarea":
      case "search":
          frm_elements[i].value = "";
          break;
      case "hidden":
          frm_elements[i].value = "";
          break;
      case "radio":
      case "checkbox":
          if (frm_elements[i].checked)
          {
              frm_elements[i].checked = false;
          }
          break;
      case "select-one":
          frm_elements[i].selectedIndex = 0;
          break;
      case "select-multi":
          frm_elements[i].selectedIndex = -1;
          break;
      default:
          break;
      }
  }
}


