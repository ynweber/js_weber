function catchForm(source_elem, successFunction, successVar, serial) {
  console.log('catchForm');
  console.log(serial);
  // it was running twice when catchForm was called twice.
  // .off() seems to have fixed it
  $(source_elem).off( 'submit' );
  url = $(source_elem).attr('action')+'?turbolinks=0';
  console.log(url);
  
  $(source_elem).submit(function(event) {
    event.preventDefault();
    $.ajax({
      url : url,
      type: "GET",
      data: serial,
      success: function(data) {
        successFunction(successVar, data);
      }
    });
  });
}
